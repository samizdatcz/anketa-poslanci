function Rendruj(data) {
  $("#anketa" ).empty();
  $(data).each(function(i) {
      if (this.v.length>0) {$("<a class='open-popup-link' href='#popup-" + i + "'><div class='respondent'></div></a>").appendTo("#anketa")};
      if (this.v.length>0) {$(".respondent").last().append("<div class='cedulka'>Klikněte pro celou odpověď</strong></div>")};
      if (this.v.length==0) {$("<div class='respondent neodpovedel'></div>").appendTo("#anketa")};
      $(".respondent").last().append("<img class='portret' src='https://samizdat.blob.core.windows.net/storage/anketa-poslanci-dos/" + this.f + "'>");
      $(".respondent").last().append("<p class='jmeno'><strong>" + this.j + " " + this.p + "</strong>, " + this.s + "</p>");
      if (this.v.length>0) {$(".respondent").last().append("<p class='veta'>&bdquo;" + this.v + "&ldquo;</p>")};
      if (this.v.length==0) {$(".respondent").last().append("<p class='veta' style='color:red;'>Bez odpovědi</p>")};
      $(".respondent").last().append("<div class='white-popup mfp-hide' id='popup-" + i + "'><p><strong>" + this.j + " " + this.p + "</strong>, " + this.s + "</p><p><em>1. Proč jste se rozhodl/a ve volbách kandidovat?</em></p><p>" + this.o1 + "</p><p><em>2. Co bude vaším hlavní tématem ve sněmovně?</em></p><p>" + this.o2 + "</p><p><em>3. Co vaše dosavadní práce, vzdáte se jí?</em></p><p>" + this.o3 + "</p><p><em>4. Jaké jsou vaše současné majetkové poměry?</em></p><p>" + this.o4 + "</p><p><em>5. Na jak dlouhou přestávku má každý jednací den sněmovny nárok každý poslanecký klub?</em></p><p>" + this.o5 + "</p><p><em>6. Jste pro setrvání v Evropské unii?</em></p><p>" + this.o6 + "</p></div>");
    });

    $(".cedulka").hide();

    $(".respondent").hover(
      function() {
        $(this).addClass("vybrany");
        $(this).find(".cedulka").show();
      }, function() {
        $(this).removeClass("vybrany");
        $(this).find(".cedulka").hide();
      }
    );
    
    $('.open-popup-link').magnificPopup({
      type:'inline',
      midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });
}

$(document).ready(function() {
  $.getJSON( "https://interaktivni.rozhlas.cz/data/anketa-poslanci/data/data.json", function(data) {
    Rendruj(data);   

    $('#vyber').on('change', function() {
       var strana = this.value;
       var data2 = $.grep(data, function(n, i) {
            if (strana=="vsechny") {return(data)} else {return(n.s.indexOf(strana)!==-1)}
       });
       Rendruj(data2);
    });
  });
});