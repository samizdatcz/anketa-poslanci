title: "123 nováčků v poslaneckých lavicích. Někteří kumulují funkce, jiní odmítají odpovídat na otázky"
perex: "Nechtěli prý jen sedět a stěžovat si. V Poslanecké sněmovně, kam od roku 1996 usedne vůbec nejvíc nových zákonodárců, se tak chtějí věnovat například e-governmentu, životnímu prostředí či školství. Další z nováčků se ale omezili pouze na vyjádření, že chtějí prosazovat program strany. Server iROZHLAS.cz oslovil všech 123 nových poslanců a ptal se na jejich motivaci kandidovat, čemu se chtějí věnovat, ale i na jejich majetkové poměry nebo vztah k Evropské unii. Jak odpovídali, a kdo se vykrucoval?"
authors: ["Hana Mazancová", "Kristýna Novotná"]
published: "15. listopadu 2017"
coverimg: ""
coverimg_note: "" 
styles: []
libraries: ["jquery"] 
options: noheader, nopic
---

##Otázky:
 
1. Proč jste se rozhodl/a ve volbách kandidovat?
2. Co bude vaším hlavní tématem ve sněmovně?
3. Co vaše dosavadní práce, vzdáte se jí?
4. Jaké jsou vaše současné majetkové poměry?
5. Na jak dlouhou přestávku má každý jednací den sněmovny nárok každý poslanecký klub?
6. Jste pro setrvání v Evropské unii?

Celé odpovědi zobrazíte kliknutím na vybraného poslance či poslankyni.

<wide>
<label for="vyber">Poslanci zvolení za</label>
<select id="vyber">
  <option value="vsechny" selected>všechny strany</option>
  <option value="ANO">ANO</option>
  <option value="ODS">ODS</option>
  <option value="Piráti">Piráty</option>
  <option value="SPD">SPD</option>
  <option value="KSČM">KSČM</option>
  <option value="ČSSD">ČSSD</option>
  <option value="KDU-ČSL">KDU-ČSL</option>
  <option value="TOP 09">TOP 09</option>
  <option value="STAN">STAN</option>
</select>
<div id="anketa"></div>
<wide>


Poslanecký klub složený ze samých nováčků bude mít ve sněmovně Česká pirátská strana. Ta pod vedením předsedy Ivana Bartoše obsadí celkem 22 křesel.
 
Zájem o e-government, elektronizace státní správy, větší transparentnost ve fungování státu. To je společný jmenovatel, který se objevuje téměř ve všech odpovědích Pirátů. Na dotazy serveru iROZHLAS.cz odpovídali prakticky okamžitě, bez snahy získat otázky předem či odpovědět po e-mailu.

##Ojetá toyota
 
A stejně tak Pirátům nevadilo odhalit se, i pokud jde o majetek. Devětatřicetiletý poslanec Martin Jiránek například tvrdí, že do politiky kvůli „vilám za dvacet milionů a autům za tři miliony“ nešel.

<right>Další z Pirátů uvádí, že svého občanského zaměstnání se budou muset vzdát. "V tuto chvíli pracuji jako manažer informačních technologií ve vývoji softwaru. Vzhledem k tomu, že na své pozici dávám práci poměrně hodně lidem, tak jsem se rozhodl, že to angažmá ukončím. Nedává smysl kombinovat ho s prací poslance. Této práci se budu věnovat na plný úvazek," říká například poslanec Jan Lipavský.</right>

„Já vlastně nic nemám. Mám jeden byt, jedno malé auto a to je tak všechno,“ odpověděl na dotaz serveru iROZHLAS.cz, jaké jsou jeho současné majetkové poměry. Stručné přiznání majetku podle zákona o střetu zájmů pak bude mít zřejmě i Vojtěch Pikal, kandidát na místopředsedu dolní komory. „Mám pár tisíc na účtu a to je všechno,“ popsal. Jeho stranický kolega Jan Pošvář zase do majetkového přiznání zahrne „ojetou toyotu asi 17 let starou“.
 
Možná vůbec poprvé se v majetkových přiznáních podle zákona o střetu zájmů objeví investice do virtuálních měn. Tam investoval Pirát Tomáš Vymazal, kterému rodiče v posledním roce věnovali 400 tisíc korun. „Z nich polovina padla na kampaň, druhou polovinu jsem vrazil do přístrojů na těžbu kryptoměn,“ popsal.
 
Někteří z Pirátů už také mají jasno ohledně současného zaměstnání. Vzdá se ho například jednadvacetiletý Pirát František Kopřiva, spolu s poslancem Dominikem Ferim (TOP 09) vůbec nejmladší člen dolní komory. Do zaměstnání přitom nastoupil před dvěma měsíci.
 
„Teď musím práci opustit, abych to zvládal. Ve škole nyní řeším, jak to budu dělat. Asi individuální studijní plán, případně se domluvit s vyučujícími, že budu mít více absencí, když bude zasedat sněmovna,“ popisuje v anketě své plány.
 
Naopak do práce v Poslanecké sněmovně se Pirátka Olga Richterová pustí po mateřské dovolené. „Která mi skončí, až najdu chůvu,“ říká. Ponechat si chce také neuvolněný mandát zastupitele na Praze 10.
 
##Sběrači funkcí
 
Nejvíce nováčků přichází do Poslanecké sněmovny v dresu hnutí ANO. Velká část z dvaapadesáti nových zákonodárců by se přitom ráda věnovala problematice zdravotnictví, životního prostředí nebo zemědělství a regionálnímu rozvoji. Vyplývá to alespoň z jejich odpovědí v anketě serveru iROZHLAS.cz.

<left>„Dělám pětadvacet let ředitele úspěšné firmy, tak asi něco vím o hospodářství. Takže hospodářský výbor,“ nastínil miliardář Juříček, čemu by se chtěl ve sněmovně věnovat. Bláha se zase chystá hájit zájmy podnikatelů: „Vydají se nějaké zákony a nikoho nezajímá, že ten podnikatel se to vůbec nedozvěděl a že si s tím neví rady.“</left>
 
„Docela úspěšně jsem se zabýval cenou vody a nakládáním s investicemi do vodovodní sítě,“ popsal třeba Radek Zlesák, který současně působí jako radní ve Žďáru nad Sázavou. Naopak energetice se chce věnovat dosavadní zastupitelka v Náměšti nad Oslavou Monika Oborná: „Jelikož jsem byla zvolena za Vysočinu, je pro nás rozhodně velké téma dostavby jaderného bloku v Dukovanech.“ A proč se nejmladší poslankyně hnutí rozhodla kandidovat? „Mladí lidé do politiky prostě patří,“ řekla Oborná.
 
Hnutí ANO bude ve sněmovně reprezentovat i několik podnikatelů známých tváří: bývalý hokejista Milan Hnilička, exmoderátor Aleš Juchelka, pekař Jiří Bláha nebo miliardář Pavel Juříček.
 
Vedle toho byla v případě zástupců hnutí ANO na místě otázka na to, jak se vypořádají - vzhledem k poslaneckému mandátu - se svými dalšími závazky. U řady z nich totiž nyní dochází ke kumulaci až tří volených funkcí.
 
Například Přemysl Mališ sedí v zastupitelstvu v Černošicích a i na kraji, funkcí se ale vzdát neplánuje. „Domnívám se, že pokud se vzdám zaměstnání, tak se to dá skloubit, neboť nemám výkonné funkce v krajském ani obecním zastupitelstvu,“ prohlásil. Podobně mluví i náměstkyně jihomoravského kraje Taťána Malá: „Celou volební kampaň jsem říkala, že nejsem sběratel funkcí, ale byla bych ráda, kdybych si mohla tyto dvě funkce ponechat.“
 
Poslancem a hejtmanem současně zatím zůstává i Ivo Vondrák: „Já jsem se ptal bývalých i současných poslanců a hejtmanů, jako je pan Běhounek (hejtman Vysočiny za ČSSD – pozn. red.), zda-li se to dá stihnout, a on tvrdí, že se to dá.“
 
##Mluvčí Okamura
 
Samostatnou kapitolu tvoří komunikace se zástupci hnutí Svoboda a přímá demokracie Tomia Okamury. Z 19 nových poslanců, které redakce oslovila, se totiž podařilo získat odpovědi jen od čtyř z nich. Ankety se zúčastnila třeba Jana Levová, která loni na facebooku vypustila falešnou zprávu o běžencích na Plzeňsku. Jaké bude její téma ve sněmovně a co plánuje prosadit? „SPD si zakládá na tom, aby prosazovalo svůj volební program.
Takže určitě volební program,“ řekla v anketě.
 
Na změně si zakládá i její nová kolegyně z Ústecka Tereza Hyťhová. I proto se rozhodla kandidovat ve volbách do sněmovny. „Zastávám názor, že když člověk sedí doma, tak toho moc nezmění,“ popsala učitelka a vysokoškolská studentka.

<right>Část poslanců včetně Miloslava Roznera, který zastupoval hnutí v předvolební debatě v České televizi na téma kultura, přitom s rozhovorem souhlasila. Rozner přislíbil, že po jednání zavolá zpátky, už tak ale neučinil a neozval se ani na další volání. Specifický byl případ Karly Maříkové, původní profesí zdravotní sestry. Ta redakci rozhovor slíbila hned několikrát, v dohodnutý čas ale telefon nikdy nezvedla. Na domluvený termín rozhovoru a ani na následné SMS zprávy a telefonáty nereagovala ani její stranická kolegyně Monika Jarošová.</right>
 
Poslanci SPD v anketě zodpovídali i dotaz na majetkové poměry. „Pokud se ptáte na hodnoty, tak je to opravdu dost,“ nastínil například Pavel Jelínek, který donedávna podnikal v informačních technologiích. Jako drobný podnikatel působil donedávna také Lubomír Španěl. Živnost ale ukončil ještě dříve, než se zavedla elektronická evidence tržeb. „Ne že bych se toho bál, ale změn a stresu bylo hodně, k tomu nestabilita prostředí,“ vysvětlil.
 
Zbytek čerstvých poslanců SPD na snahu o kontaktování telefonicky i SMS zprávou nereagoval nebo rozhovor redakci dát odmítl. Důvod? Nešťastné mediální výstupy některých členů hnutí před volbami, jak serveru iROZHLAS.cz řekl Zdeněk Podal. „To se právě stalo asi ve dvou případech, kdy to začalo takhle nevinně a ten člověk ve své dobráckosti nebo naivitě… Znáte novináře, oni potom zneužili to, co v dobrém myslel.“ Na jednání poslaneckého klubu tak podle Podala došlo k dohodě, že za hnutí bude zatím mluvit pouze předseda Okamura a šéf poslanců SPD Radim Fiala. „Po tuto hektickou dobu nechceme žádným způsobem znevýhodňovat pozici našeho vedení,“ doplnil Podal.
 
##Zbytky volného času

Pomoct nejen své straně TOP 09, ale zejména přenášet názory mladých lidí, kteří byli jeho nejčastějšími voliči. To si zase klade za cíl nejmladší poslanec Dominik Feri. Student třetího ročníku právnické fakulty by se také rád viděl v ústavně právním výboru. „Se studiem tato práce dává smysl,“ říká. S omezením svých pracovních aktivit počítá jeho stranický kolega a lékař Vlastimil Válek. Jak ale přiznává, rozhodně si chce ponechat práci na lékařské fakultě i ve fakultní nemocnici. A to kvůli „kritice kolegů, studentů i spolupracovníků“.

Poslanecký mandát, jak přiznává v anketě, překvapil Janu Krutákovou. Jednička hnutí STAN Jihomoravského kraje má zkušenosti z komunální politiky, mimo jiné i proto se rozhodla zkusit tu celostátní. „Po poradě v rodině jsem kývla na kandidaturu,“ popisuje.
 
Podobné zkušenosti z komunální politiky chce ve sněmovně využít i nováček za komunisty Daniel Pawlas. „Jsou mi blízká města, obce, vesnice,“ vyjmenovává bývalý primátor města Havířova, na co by se rád zaměřil. A mezi nováčky je ve sněmovně i dlouholetá politička a senátorka za sociální demokracii Alena Gajdůšková. Z křesla bývalé ministryně školství se pak do sněmovny dostala i Kateřina Valachová. „Hlavní impulz byl dokončit svou práci v oblasti vzdělávání,“ vysvětluje v anketě, proč se rozhodla kandidovat. Chce se věnovat nejen školství a vzdělávání, a coby právnička také justici.

Na kandidaturu do Senátu před dvěma lety navázal nyní Pavel Žáček. A uspěl. Historik a zakladatel a první ředitel Ústavu pro studium totalitních režimů kandidoval za ODS. „Nabídl jsem své zkušenosti z oblasti bezpečnosti armády. Pak samozřejmě moje velmi důležité téma je otázka paměťových institucí a otázka vyrovnání se s minulostí,“ říká. Na anketní otázky však odmítl odpovídat jeho partajní kolega Václav Klaus mladší. „Rozhovory nyní neposkytuji,“ uvedl stručně.

Za KDU-ČSL se do dolní komory dostal starosta Prahy 7 Jan Čižinský, kterého do sněmovny vyneslo právě kroužkování. Složité rozhodování ho ale nečeká, funkce si totiž ponechá. „Já jsem slíbil, že se nic nezmění, že zůstanu starostou. Mimo jiné i proto, že kroužky, které mě do sněmovny dostaly, byly většinou z Prahy 7,“ říká.